from fractions import Fraction
from math import gcd
from functools import reduce

float_state = False

class Task:
    def __init__(self, wcet, period, deadline):
        self.wcet = wcet
        self.period = period
        self.deadline = deadline
        self.execution_time = 0
        self.running = False

    def start_execution(self):
        self.execution_time = self.wcet

    def decrement_execution(self, step_value=1):
        if self.execution_time > 0:
            self.execution_time -= step_value
        if self.execution_time <= 0:
            self.running = False

    def is_ready(self):
        return self.execution_time > 0

class Floating_Task:
    def __init__(self, wcet, period, deadline):
        self.wcet = wcet
        self.period = period
        self.deadline = deadline
        self.execution_time = 0.0
        self.running = False

    def start_execution(self):
        self.execution_time = self.wcet

    def decrement_execution(self, step_value=0.1):
        if self.execution_time > 0.0:
            self.execution_time -= step_value
        if self.execution_time <= 0.0:
            self.running = False

    def is_ready(self):
        return self.execution_time > 0.0

def lcm(a, b):
    return abs(a * b) // gcd(a, b)

def lcm_multiple(numbers):
    return reduce(lcm, numbers)

def find_step_size(periods):
    fractions = [Fraction(period).limit_denominator(1000).denominator for period in periods]
    step_size = float(1 / lcm_multiple(fractions))
    return step_size

def simulate_rm(tasks):
    n = len(tasks)
    periods = [task.period for task in tasks]
    wcets = [task.wcet for task in tasks]
    deadlines = [task.deadline for task in tasks]
    hyperperiod = lcm_multiple(periods) if not float_state else max(periods)
    
    preemption_counts = [0] * n
    current_task = None
    step_value = find_step_size(wcets) if float_state else 1
    
    t = 0.0

    while t < hyperperiod:
        for i, task in enumerate(tasks):
            if t % task.period == 0:
                task.start_execution()

        highest_priority_task = None
        for i, task in enumerate(tasks):
            if task.is_ready() and (t + task.execution_time) <= (task.deadline + (t // task.period) * task.period):
                if highest_priority_task is None or task.period < tasks[highest_priority_task].period:
                    highest_priority_task = i

        if highest_priority_task is not None:
            if current_task is not None and current_task != highest_priority_task:
                if tasks[highest_priority_task].period < tasks[current_task].period:
                    if tasks[current_task].is_ready() and tasks[current_task].running:
                        preemption_counts[current_task] += 1

            current_task = highest_priority_task
            tasks[current_task].running = True
        
        if current_task is not None:
            tasks[current_task].decrement_execution(step_value)
            if not tasks[current_task].is_ready():
                tasks[current_task].running = False

        t += step_value

    return preemption_counts

def read_tasks_from_file(filename):
    tasks = []
    global float_state
    try:
        with open(filename, 'r') as file:
            for line in file:
                wcet, period, deadline = map(int, line.strip().split(','))
                tasks.append(Task(wcet, period, deadline))
    except ValueError:
        tasks = []
        with open(filename, 'r') as file:
            for line in file:
                wcet, period, deadline = map(float, line.strip().split(','))
                tasks.append(Floating_Task(wcet, period, deadline))
        float_state = True

    return tasks

def main(filename):
    tasks = read_tasks_from_file(filename)
    utilization = sum(task.wcet / task.period for task in tasks)
    bound = 1
    feasible = 1 if utilization <= bound else 0
    print(feasible)
    if feasible == 1:
        preemption_counts = simulate_rm(tasks)
        print(','.join(map(str, preemption_counts)))

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print("Usage: python3 ece_455_final.py <tasks_file>")
    else:
        main(sys.argv[1])

